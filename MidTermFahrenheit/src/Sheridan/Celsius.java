package Sheridan;

public class Celsius {
	
	
    public static int convertFromFahrenheit(int fahrenheit)
    {
        int celsius = (int) Math.ceil(((fahrenheit - 32)*5) / 9);
        
        return celsius;
        
    }

    public static void main(String [] args)
    {
        int result = convertFromFahrenheit(20);
    
        System.out.println(result);
    }
}
