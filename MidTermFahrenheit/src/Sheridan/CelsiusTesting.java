package Sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CelsiusTesting {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConvertFromFahrenheit() {
		assertEquals(0,Celsius.convertFromFahrenheit(32));
	}
	@Test
	public void testConvertFromFahrenheitBoundryIn() {
		assertEquals(-15,Celsius.convertFromFahrenheit(5));
	}
	@Test
	public void testConvertFromFahrenheitBoundryOut() {
		assertEquals(-14,Celsius.convertFromFahrenheit(6));
	}
	@Test
	public void testConvertFromFahrenheitNegative() {
		assertNotEquals(-1,Celsius.convertFromFahrenheit(32));
	}

}